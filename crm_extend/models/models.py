# -*- coding: utf-8 -*-

from odoo import models, fields, api


class crm_extend(models.Model):
    _inherit = "crm.lead"

    coach_id = fields.Many2one('res.users', string="Partner Coach")

    def action_call_partner_from(self):
        return {
            'name': 'Create Partner',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'context': {'default_phone': '+90 ', 'default_name': self.name},
            'res_model': 'res.partner',
            'target': 'new',
        }

    def action_assign_user(self):
        for record in self:
            record.coach_id = self.env.user.id
            record.partner_id = self.env.ref('crm_extend.res_partner_default_partner').id

        return {
            'name': 'Wizard',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'context': {'default_phone': '+90'},
            'view_id': self.env.ref('crm_extend.view_sample_wizard').id,
            'res_model': 'sample.wizard',
            'target': 'new',
        }



