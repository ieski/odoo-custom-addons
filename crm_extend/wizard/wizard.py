# -*- coding: utf-8 -*-

import logging
import time

from odoo import models, fields
from odoo import tools
from odoo.exceptions import ValidationError, UserError

_logger = logging.getLogger(__name__)


class Sample_Wizard(models.TransientModel):
    _name = 'sample.wizard'
    _description = 'Sample Wizard'

    date_from = fields.Date('Date', default=lambda *a: time.strftime('%Y-%m-%d'), required=True)
    name = fields.Char("Name")
    phone = fields.Char("Phone")

    def save(self):

        try:

            data = self.read()[0]
            date_from = data['date_from']
            name = data['name']
            phone = data['phone']

            partner = {
                "name": name,
                "accept_date": date_from,
                "phone": phone
            }

            self.env['res.partner'].create(partner)


        except Exception as e:
            _logger.error('PArtner Create Error: %s', tools.ustr(e))
            raise UserError(tools.ustr(e))
