# -*- coding: utf-8 -*-

from odoo import models, fields, api


class sample_table(models.Model):
    _name = 'sample_module.sample_table'
    _description = 'Sample description'

    name = fields.Char("Name", required=True)
    price_date = fields.Date('Price Date')

    price = fields.Float('Price')
    currency_id = fields.Many2one('res.currency', readonly=True, default=lambda x: x.env.company.currency_id)

    tax = fields.Float(compute="_tax", store=True)
    sale_type = fields.Selection(
        [('cash', 'Cash'),
         ('credi', 'Credi')
         ],
        'Type', required=True)

    partner_id = fields.Many2one('res.partner', string='Partner', required=True, domain=[('is_company', '=', True)])
    description = fields.Text()
    children_ids = fields.One2many('sample_module.sample_table_line', 'parent_id', string="Line")

    @api.depends('price')
    def _tax(self):
        for record in self:
            record.tax = float(record.price) * 18 / 100

    @api.onchange('price_date')
    def _onchange_partner_price_date(self):
        if self.name:
            self.name += " (changed)"

    @api.model
    def create(self, values):

        if 'children_ids' in values:
            price = 0.0
            for item in values['children_ids']:
                price += item[2]['amount']
            values['price'] = price

        res = super(sample_table, self).create(values)
        return res

    def write(self, vals):
        if 'children_ids' in vals:
            price = 0.0
            for item in vals['children_ids']:
                price += item[2]['amount']
            vals['price'] = price

        res = super(sample_table, self).write(vals)
        return res


class sample_table_line(models.Model):
    _name = 'sample_module.sample_table_line'
    _description = 'Sample description'

    amount = fields.Monetary('Amount', currency_field='currency_id')
    currency_id = fields.Many2one('res.currency', readonly=True, default=lambda x: x.env.company.currency_id)
    parent_id = fields.Many2one('sample_module.sample_table')
    comment = fields.Char("Comment")
