# -*- coding: utf-8 -*-

from odoo import api, models


class TestReport(models.AbstractModel):
    _name = 'report.sample_module.sample_table_record'

    @api.model
    def _get_report_values(self, docids, data=None):
        docs = self.env['sample_module.sample_table'].browse(docids)
        
        return {
            'doc_ids': docs.ids,
            'doc_model': 'sample_module.sample_table',
            'docs': docs,
            'tax_calculation': self.tax_calculation()
        }

    def tax_calculation(self):
        pass